module.exports = function(element, array) {
    return array.indexOf(element) !== -1;
};
